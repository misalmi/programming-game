                                                                     
                                                                     
                                                                     
                                             
12:41:32 < mdf> 14:57:31 < mdf> pohdiskelenpa t�h�n mahdollisia seuraavan sprintin juttuja:
12:41:32 < mdf> 14:58:21 < mdf> - vois olla toimiva liikkuminen nodejen v�lill� ja vain nodejen v�lill�
12:41:32 < mdf> 14:58:44 < mdf> - vois olla pitk�lti olemassa ne erityyppiset blokit
12:41:32 < mdf> 14:59:01 < mdf>     * ei ole pakko toimia viel�
12:41:32 < mdf> 14:59:13 < mdf> - inventory
12:41:32 < mdf> 14:59:24 < mdf> - grafiikat niille erityyppisille blokeille
12:41:32 < mdf> 14:59:30 < mdf>     * ainaki alustavat
12:41:32 < mdf> 15:00:04 < mdf> - ehk� jotain alustavaa dokumentointia?
12:41:32 < mdf> 15:00:10 < mdf> - alustavia testej�?
12:53:43 < mdf> mulle todo: taskien pilikkomista, placeholdereiden v��nt�

Erityyppiset blokit:

pass
- tyhj�

for ja endfor
- iteraattori
- alusta saa iteraattorista uuden muuttujan
- teleportit p�iss�
    * sammuvat kun iteraattori on tyhjentynyt
- v�rj�ytynyt maa matkan varrelta? r��ri/piuha alusta loppuun?

if (ja elseif)
- branchays hisseill� inventoryn sis�ll�n mukaan

funktio
- putki maassa, alas menem�ll� callstackiin pushataan uusi funktio
    * siirryt��n uuden funktion nodepuuhun
    * otetaan uusi, tyhj� inventory k�ytt��n (pit�en silm�ll� vanhan k�ytt�misen parametrein�)

muuttujan alustus/kasvatus
- maan p��ll� hengaava esine, kuten kirsikka
- alustaa kyseisen esineen m��r�n callstackin p��llimm�isest� inventoryst� arvolla 1 tai korottaa yhdell�
- muuttuu pass-blokiksi aktivoimisen j�lkeen

ohjelman lopetus
- kent�n oikeassa laidassa joku tunneli
- p��see sis��n kun muuttujilla on tietyt arvot





Inventory:
- dictionary esineist� ja niiden lukum��rist�
- oma jokaiselle kohdalle callstackissa
- konstruktointi: tyhj�n� tai annettujen parametrien mukaan
    * mahdollistaa esim. kirsikan antamisen parametrin�

Esineet:
- nimi, lukum��r�, grafiikka
- ei kummempaa funktiota
- pit�isik� olla global-fl�gi? halutaanko sit� opettaa?
    * n�kyisi callstackin jokaisella tasolla
    * ymp�r�ity jollain tahmealla v�rill�?

    
    
    
    
    
Teko�ly:
- 1. aktivoi nykyinen, 2. liiku yksi oikealle
- jos mit��n ei tapahdu (ts. koordinaatit, callstackin nyk. funktio eik� inventory muutu), kuole







Alkuvalikko, k�ytt�liittym�:
- gui, jossa klikattavia nappeja testin vuoksi vaikka pari
    * play





Kentt�editori





Formaatti, jossa voi tallentaa vaikka jsonina kokonaisen kent�n pelaamista
varten
- loaderi, joka lataa t�mm�sen pelattavaksi
- joku, jolla tallentaa n�it�





Testit:
- unit testej�. testikoodia, joka kokeilee luotujen luokkien metodeja
    * kutsutaan tietyill� parametreill� metodia
    * katsotaan, tuliko se mit� odotettiinki





Piirrett�v��:
- kaikki grafiikka samaan tiedostoon
- l�pin�kyvyydet kuntoon
- inventory frame
    * miten esineet on ymp�r�ity? py�ristetyt kulmat?
    * miss� kohti lukum��r� n�kyy ja miten?
    * tarvitaanko taustalle l�hes mustat haamuesineet?
    * milt� n�ytt�� valittu/aktiivinen/vasta poimittu esine? eriv�riset feidaavat reunat?
- hissi
    * hissin alla oleva tile? hissin koneisto?
    * hissin runko
    * hissin hytti/kori - n�m� voisivat liikkua aina kent�ll� samalle tasolle, jolla pelaaja on, jos ollaan kyseisen iffijutskan sis�ll�
    * venyv� k�ysi/vaijeri hissikorin yl�puolelle pelaajan taustalle?
    * alhaalta yl�s ty�ntyv� venyv� hydrauliikka hissikorin alapuolelle?
    * hissin ylimm�lle tasolle oma grafiikka?
- for
    * iteraattori ekan palikan ylle, iteraattorin sis��n/taustalle iteraattorin seuraava esine
    * forin sis�iset passtilet jotenkin eri tavalla, jotta forin scope erottuu?
    * teleporttereihin hohtava animaatio? sammunut teleportteri erikseen!
    * teleporttereiden ylle kimaltelevia asioita, nousevia ulostuloon, laskevia sis��nmenoon
- enemm�n esineit�:
    * kirsikka
    * banaani
    * p��ryn�

